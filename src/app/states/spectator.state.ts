import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ConnectionService} from '../services/connection.service';
import Player from '../models/player.model';

@Injectable({
  providedIn: 'root'
})
export class SpectatorState {
  connected$ = new BehaviorSubject<boolean>(false);
  players$ = new BehaviorSubject<Player[]>([]);
  currentGame$ = new BehaviorSubject<number>(1);
  currentFrames$ = new BehaviorSubject(0);
  totalGames$ = new BehaviorSubject(1);

  delay = 0;

  constructor(private connectionService: ConnectionService) {
  }

  connect() {
    this.connectionService.connect().then(() => {
      this.connectionService.getMessages().subscribe(() => this.connected$.next(true));
    });
  }

  disconnect(centerId) {
    this.connectionService.disconnect(centerId);
  }

  startSpectatingMatch(matchId) {
    this.connected$.subscribe(connected => {
      if (connected) {
        this.subscribeMatch(matchId);
        this.processMessages();
      }
    });
  }

  processMessages() {
    this.connectionService.getMessages().subscribe(val => {
      const parsedData = JSON.parse(val.data);

      if (parsedData.result?.leaderBoard) {
        const players = parsedData.result?.leaderBoard as Player[];
        this.setPlayers(players.slice(0, 12));

        // const totalGames = parsedData.result?.totalGames;
        this.totalGames$.next(parsedData.result?.totalGames);
        this.currentGame$.next(this.totalGames$.value);
      }

      if (parsedData.result?.data?.player) {
        const player = parsedData.result?.data?.player as Player;

        const pos = this.players$.value.findIndex(pl => pl.playerName === player.playerName);

        if (pos > -1) {
          const temp = [...this.players$.value];
          temp[pos] = player;
          this.setPlayers(temp);
        } else {
          if (this.players$.value.length < 12) {
            const temp = [...this.players$.value, player];
            this.setPlayers(temp);
          }
        }
      }

      this.setCurrentFrame();
    });
  }

  setCurrentFrame() {
    let tempFrame = 100;
    this.players$.value.forEach(p => {
      tempFrame = Math.min((p.games[this.totalGames$.value - 1]?.scores?.length) || 100, tempFrame);
    });
    this.currentFrames$.next(Math.floor((tempFrame - 1) / 3) * 3);
  }

  setPlayers(players: Player[]) {
    const applyPlayers = () => {
      players.sort((a, b) => a.position - b.position);
      this.players$.next(players);
    };
    if (this.delay) {
      setTimeout(applyPlayers, this.delay);
    } else {
      applyPlayers();
    }
  }

  setDelay(val) {
    this.delay = val;
  }

  subscribeMatch(matchId) {
    this.connectionService.subscribeMatch(matchId);
  }

  unsusbscribeMatch(matchId) {
    this.connectionService.unsubscribeMatch(matchId);
  }
}
