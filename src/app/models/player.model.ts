export default class Player {
  playerName: string;
  games: {
    scores: string[]
  }[];
  position: number;
  score: number;
}
