import {Component, ViewEncapsulation} from '@angular/core';
import {ConnectionService} from '../services/connection.service';
import {SpectatorState} from '../states/spectator.state';
import Player from '../models/player.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent {
  ViewMode = 'Normal';
  players$ = this.spectatorService.players$;
  currentGame$ = this.spectatorService.currentGame$;
  currentFrames$ = this.spectatorService.currentFrames$;
  gamesArray = [];

  groups = {};
  displayNames = {};
  selectedGame = '';
  startFrame = -1;
  framesArray = new Array(10);

  delay = 0;


  constructor(private spectatorService: SpectatorState, private route: ActivatedRoute) {
    this.route.queryParamMap.subscribe(queryParams => {
      const matchId = queryParams.get('matchId');
      if (matchId) {
        spectatorService.connect();
        spectatorService.startSpectatingMatch(Number(matchId));
      }
    });

    this.spectatorService.totalGames$.subscribe(tot => {
      if (tot) {
        this.gamesArray = new Array(tot);
      }
    });
  }

  getPlayerName(name: string) {
    if (this.displayNames[name]) {
      name = this.displayNames[name];
    }
    const nameLength = 20;
    if (name.length > nameLength) {
      name = name.substr(0, nameLength) + '.';
    }

    return name;
  }

  getCurrentFrames(pos: number) {
    return (Number(this.startFrame) > -1 ? Number(this.startFrame) : this.currentFrames$.value) + pos;
  }

  getPlayerScore(player: Player, pos: number) {
    return player.games[(Number(this.selectedGame) || this.currentGame$.value) - 1]?.scores[this.getCurrentFrames(pos)];
  }

  getPlayerLastThrow(player: Player) {
    return player.games[(Number(this.selectedGame) || this.currentGame$.value) - 1]?.scores.length - 1;
  }

  applyDelay() {
    this.spectatorService.setDelay(this.delay);
  }
}
