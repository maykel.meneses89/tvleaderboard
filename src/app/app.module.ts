import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MainComponent } from './main/main.component';
import {CommonModule} from '@angular/common';
import {ConnectionService} from './services/connection.service';
import {WebSocketService} from './services/web-socket.service';
import {SpectatorState} from './states/spectator.state';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot([])
  ],
  providers: [ConnectionService, WebSocketService, SpectatorState],
  bootstrap: [MainComponent]
})
export class AppModule { }
