import {Injectable} from '@angular/core';
import {WebSocketService} from './web-socket.service';
import {Subject} from 'rxjs';
import {Config} from '../config';
import {v1 as uuidv1} from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private websocketUrl = Config.WEBSOCKET_URL;
  private messages: Subject<any>;
  private state = '';

  constructor(private websocketService: WebSocketService) {
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.websocketService.connect(this.websocketUrl).then(() => {
        this.messages = this.websocketService.getMessages();

        // connect to server?
        this.messages.next({
            id: uuidv1(),
            method: 0,
            params: {
              api_key: Config.API_KEY
            }
          }
        );
        this.state = 'connected';
        resolve();
      });
    });
  }

  subscribeMatch(matchId) {
    if (this.state === 'connected' || this.state === 'subscribed') {
      this.messages.next({
          id: uuidv1(),
          method: 10,
          params: {
            matchId
          }
        }
      );
      this.state = 'subscribed';
    }
  }

  unsubscribeMatch(matchId) {
    if (this.messages) {
      this.messages.next({
          id: uuidv1(),
          method: 10,
          params: {
            matchId
          }
        }
      );
    }
    this.state = 'connected';
  }

  getMessages(): Subject<any> {
    return this.messages;
  }

  disconnect(centerId) {
    this.websocketService.disconnect();
  }
}
