import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

@Injectable()
export class WebSocketService {
  private socket: WebSocket;

  constructor() {
  }

  private subject: Rx.Subject<MessageEvent>;

  public async connect(url): Promise<any> {
    return new Promise((resolve, reject) => {
      this.socket = new WebSocket(url);

      this.socket.addEventListener('open', (event) => {
        resolve();
      });

      this.socket.addEventListener('error', () => {
        reject();
      });

      this.socket.onclose = event => {
        alert('socket closed');
        this.socket = null;
      };
    });
  }

  public getMessages(): Rx.Subject<MessageEvent> {
    if (!this.subject) {
      const ws = this.socket;

      const observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
        ws.onmessage = obs.next.bind(obs);
        ws.onerror = obs.error.bind(obs);
        ws.onclose = obs.complete.bind(obs);
        return ws.close.bind(ws);
      });

      const observer = {
        next: (data: any) => {
          if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(data));
          }
        }
      };
      this.subject = Rx.Subject.create(observer, observable);
    }

    return this.subject;
  }

  disconnect() {
    this.socket.close(1000, 'Work complete');
  }
}
